### GTAlc 3000
![GTAalc](beer-led.gif "LED")

![GTAalc](screenshot.png "GTAlc")

![GTAalc](02.jpg "GTAlc")


## Starten

```
git clone git@github.com:s3p2/gtalc.git



# OPTION 1 - lokaler arduino mit Johnny five:
# StandardFirmataPlus auf Arduino geladen
# Messergebnis des Gassensors liegen auf dem analogen Pin 0 an
# pixi-code.js:
# var MODE = "arduino"; //arduino or websocket

cd gtalc
node index.js

# -> localhost:80


# OPTION 2 - webSocket - Daten kommen von der Flasche

# pixi-code.js:
# var MODE = "websocket"; //arduino or websocket
# http://10.0.130.38/ bietet Datum AlcValue an
# index.html öffnen

```


### ToDo

- [ ] Lüfter in Flasche installieren
- [ ] Messung auch ohne Verbindung zum Netze ermöglichen
- [ ] QR Code auf Flasche anbringen
- [ ] Lösung suchen den Service unabhängig von der Umgebung (Router) aufzusetzen
