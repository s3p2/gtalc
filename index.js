var five = require("johnny-five");
var fs = require('fs');

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var port = 80;
var io = require('socket.io')(server);

// Mit diesem Kommando starten wir den Webserver.
server.listen(port, function () {
    // Wir geben einen Hinweis aus, dass der Webserer läuft.
    console.log('Webserver läuft und hört auf Port %d', port);
});

// Hier teilen wir express mit, dass die öffentlichen HTML-Dateien
// im Ordner "public" zu finden sind.
app.use(express.static(__dirname + '/'));


var con = null;
var lastValue = 2000;
var thresholdSocket = 4; // wie groß muss der Unterschied zum letzten gemessenen Wert sein damit er über die Leitung geschickt wird

io.on('connection', function (socket) {
    con = socket;
    console.log("socket: " + socket);
});


var board = new five.Board();

board.on("ready", function() {

  this.pinMode(0, five.Pin.ANALOG);
   this.analogRead(0, function(voltage) {

     if (con != null && Math.abs(voltage-lastValue)>thresholdSocket){
       con.emit('mq3', { value: voltage });
       console.log("send; " + voltage);
       lastValue = voltage;
     };

   });



  this.pinMode(0, five.Pin.INPUT);
  this.digitalRead(0, function(value) {
    //console.log(value);
  });
});
