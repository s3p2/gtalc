// Socket Gedöns


// Constants

SOCKET_IS_OPEN = false;


if (MODE == "websocket"){


  function connect() {
    webSocket = new WebSocket("ws://10.0.130.38:88");
    var webSocketData = null;

    webSocket.onmessage = function (event) {
      webSocketData = event.data;
      console.log(event.data);
      let alc_tmp = JSON.parse(event.data);
      alc = alc_tmp.AlcValue;
      callAnimations();
    };

    webSocket.onopen = function() {
      // subscribe to some channels
      console.log('Socket is open.');
      SOCKET_IS_OPEN= true;
    };

    webSocket.onclose = function(e) {
      console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
      SOCKET_IS_OPEN = false;
      setTimeout(function() {
        connect();
      }, 1000);
    };
  }

connect();

}

if (MODE == "arduino"){

  var socket = io('localhost');
  socket.on('mq3', function (data) {
      alc = data.value;
      callAnimations();
  });
}
