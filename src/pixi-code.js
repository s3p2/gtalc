var MODE = "websocket"; //arduino or websocket


// STATES
var policeActive = false;
var maxAlcWhileRecording = 0;
var recording = false;

// GLOBALS
var alc = 0;


// WEIGHTS Constants
var WEIGHT_ALC_NORMAL = 0;
var WEIGHT_ALC_MAX = 1023;
var WEIGHT_ROAD_SPEED = 10;
var WEIGHT_X_FRAMES = 0; // [0 , 2]
var WEIGHT_X_SPEED = 0.05; // [0.01 - 0.5]
var WEIGHT_TURN_SPEED = 1;
var WEIGHT_POLICE_THRESHOLD = 500;
var WEIGHT_POLICE_CAR_Y_OFFSET = 100;
var WEIGHT_CAR_ROTATION = 1.58;


// TEXTURES

var road_image_path = "/src/images/road_02.jpg";
var car_image_path = "/src/images/car.png";



//Create a Pixi Application
let app = new PIXI.Application({
  width: 1024,         // default: 800
  height: 500,        // default: 600
  antialias: true,    // default: false
  transparent: false, // default: false
  resolution: 1       // default: 1
});

//Add the canvas that Pixi automatically created for you to the HTML document
document.body.appendChild(app.view);
var road_01 = null;
var road_02 = null;
var car_01_original_x = null;
var count = 0;
var inner_count = 0;


PIXI.loader
  .add(road_image_path)
  .add(car_image_path)
  .load(setup);

function setup() {
  road_01 = new PIXI.Sprite(PIXI.loader.resources[road_image_path].texture);
  road_02 = new PIXI.Sprite(PIXI.loader.resources[road_image_path].texture);
  car_01 = new PIXI.Sprite(PIXI.loader.resources[car_image_path].texture);

  //Change the sprite's position
  road_01.x = 0;
  road_01.y = 0;
  road_02.x = 0;
  road_02.y = -road_01.height;

  // scale

  car_01.pivot.set(car_01.width/2, 0)
  car_01.rotation = WEIGHT_CAR_ROTATION;
  car_01.scale.x=0.5;
  car_01.scale.y=0.5;

  car_01.x = car_01.width+440;
  car_01.y = car_01.height/2+210;
  car_01_original_x = car_01.x;

  //Add the road_01 to the stage
  app.stage.addChild(road_01);
  app.stage.addChild(road_02);
  app.stage.addChild(car_01);
  app.ticker.add(delta => gameLoop(delta));
}


function gameLoop(delta){

  //Move the road_01 1 pixel
  road_01.y += WEIGHT_ROAD_SPEED;
  road_02.y += WEIGHT_ROAD_SPEED;

  if (road_01.y > road_01.height){
    road_01.y = -road_01.height;
  }

  if (road_02.y > road_02.height){
    road_02.y = -road_02.height;
  }
  count = count + 1;

  if (count > WEIGHT_TURN_SPEED){
    count = 0;
    inner_count = inner_count+WEIGHT_X_SPEED;

    if(alc < 10){
      // für den Fall dass die Flasche 0 sendet, soll sich dass auto nicht mehr bewegen
      car_01.x = car_01_original_x+((alc)* Math.sin(inner_count));
    }
    else{
      car_01.x = car_01_original_x+((alc - WEIGHT_ALC_NORMAL)* Math.sin(inner_count));
    }




    if (policeActive){
      car_01.rotation = WEIGHT_CAR_ROTATION + Math.sin(inner_count) *0.1;
    }


  }
};







function callAnimations(){


  if (alc > WEIGHT_POLICE_THRESHOLD && !policeActive){
    policeActive = true;
    $('#police').css('left','300px'); $('#police').css('top','500px');
    car_01.y = car_01.y - WEIGHT_POLICE_CAR_Y_OFFSET;
    WEIGHT_ROAD_SPEED = WEIGHT_ROAD_SPEED + 10;
  }

  if (alc < WEIGHT_POLICE_THRESHOLD - 50 && policeActive){
    policeActive = false;
    car_01.rotation = WEIGHT_CAR_ROTATION;
    $('#police').css('left','-400px'); $('#police').css('top','600px');
    car_01.y = car_01.y + WEIGHT_POLICE_CAR_Y_OFFSET;
    WEIGHT_ROAD_SPEED = WEIGHT_ROAD_SPEED - 10;
  }



  $('progress').attr('value',alc);
  $('.alc').text( alc);

  if (recording && maxAlcWhileRecording < alc){
    maxAlcWhileRecording =  alc;
  }

}
