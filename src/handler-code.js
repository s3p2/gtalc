  $(document).ready(function() {
  // Formular Gedöns


  $('#label-mode').text(MODE);

  $('#startRecording').click(function(){
    console.log('click');
    recording = true;

    $('#startRecording').prop( "disabled", true );
    $('#stopRecording').prop( "disabled", false );

  })

  $('#stopRecording').click(function(){
    console.log('Stop, maxAlcWhileRecording: ' + maxAlcWhileRecording);


    $('#startRecording').prop( "disabled", false );
    $('#stopRecording').prop( "disabled", true );

    // Neuer tabelleneintrag
    var currentDate = new Date();



    let gtalcStorage = localStorage.getItem('gtalc');

    if (gtalcStorage == null){
      gtalcStorage = {recordings: []};
    }
    else {
      gtalcStorage = JSON.parse(gtalcStorage);
    }

    gtalcStorage.recordings.push( [$('#input-name').val(),  currentDate.getHours() +":" + currentDate.getMinutes(), maxAlcWhileRecording ]);

    localStorage.setItem('gtalc', JSON.stringify(gtalcStorage));

  $('#alcTable').DataTable().destroy();

    $('#alcTable').DataTable( {
        data: gtalcStorage.recordings,
        columns: [
            { title: "Name" },
            { title: "Uhrzeit" },
            { title: "Pegel" },
        ]
    } );


    maxAlcWhileRecording = 0;
    recording = false;
  })

  // DataTable initalisieren



    let gtalcStorage = localStorage.getItem('gtalc');

    if (gtalcStorage != null){
      gtalcStorage = JSON.parse(gtalcStorage);
    }else{
      gtalcStorage = [];
    }

      $('#alcTable').DataTable( {
          data: gtalcStorage.recordings,
          columns: [
              { title: "Name" },
              { title: "Uhrzeit" },
              { title: "Pegel [0, 1023]" },
          ]
      } );

    // Debug handler

    $('#WEIGHT_ALC_NORMAL').on('change', function(event){
      WEIGHT_ALC_NORMAL = parseInt($('#WEIGHT_ALC_NORMAL').val());
    });

    $('#WEIGHT_ROAD_SPEED').on('change', function(event){
      WEIGHT_ROAD_SPEED = parseInt($('#WEIGHT_ROAD_SPEED').val());
      console.log("SET WEIGHT_ROAD_SPEED to: " + WEIGHT_ROAD_SPEED);
    });

    $('#WEIGHT_X_FRAMES').on('change', function(event){
      WEIGHT_X_FRAMES = parseInt($('#WEIGHT_X_FRAMES').val());
      console.log("SET WEIGHT_X_FRAMES to: " + WEIGHT_X_FRAMES);
    });

    $('#toggleDebug').on('click', function(event){
        $('#debug-form').toggleClass('hidden');

        $('#label-status-connect').text(SOCKET_IS_OPEN);
    });

    $('#WEIGHT_X_SPEED').on('change', function(event){
      WEIGHT_X_SPEED = parseInt($('#WEIGHT_X_SPEED').val());
      console.log("SET WEIGHT_X_SPEED to: " + WEIGHT_X_SPEED);
    });

    $('#resetLocalStorage').click(function(){
      localStorage.clear();
      location.reload();
    })

});
